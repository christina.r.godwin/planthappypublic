# Plant Happy

Having trouble remembering when you last watered your plant? Never forget about a plant again with customized watering & fertilizing schedules, special notes and a photo journal for your plant's progress.

Contact PlantHappyApp@gmail.com with any issues or suggestions for the app.
